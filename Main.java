import java.util.Scanner;

public class Main {

     public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Tictactoe game = new Tictactoe();
        System.out.println("Welcome to Tictactoe Game");
        game.printBoard();

        while (true) {
            System.out.println("Player " + game.getPlayerMark() + ", enter your move (row and column): ");
            int row = scanner.nextInt();
            int col = scanner.nextInt();

            if (game.placemark(row, col)) {
                game.printBoard();
                if (game.checkwin()) {
                    System.out.println("Player " + game.getPlayerMark() + " wins!");
                    break;
                }
                if (game.boardfull()) {
                    System.out.println("The game is a draw!");
                    break;
                }
                game.changeplayer();
            } else {
                System.out.println("This move is not valid");
            }
        }

        scanner.close();
    }
}
