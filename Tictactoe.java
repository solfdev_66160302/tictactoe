public class Tictactoe {

    private char[][] board;
    private char playermark;

    Tictactoe() {
        board = new char[3][3];
        playermark = 'x' ;

    }

   void initializeBoard(){
        for (int i = 0;i<3;i++){
            for (int j =0;j<3;j++){
                System.out.print(board[i][j] = '-');
            }
            System.out.println();
        }
    }

    boolean boardfull(){
        boolean isfull = true;
        for (int i = 0 ; i<3 ;i++){
            for (int j =0 ; j< 3 ; j++){
                if (board[i][j] == '-'){
                    isfull = false;
                }
            }
        }
        return isfull;
    }

    void changeplayer(){
        playermark = (playermark == 'x') ? 'o' : 'x';
    }

    boolean checkrowcol(char a1 , char a2 , char a3){
        return ((a1 != '-') && (a2 == a1) && (a2 == a3));
        
    }

    boolean checkrowwin(){
        for (int i = 0 ; i<3 ; i++){
            if (checkrowcol(board[i][0], board[1][i], board[2][i]) == true){
                return true;
            }
        }
        return false;
    }

    boolean checkcolwin(){
        for (int i=0;i<3;i++){
            if (checkrowcol(board[0][i] , board[1][i], board[2][i]) == true) {
                return true;
            }
        }
        return false;
    }

    boolean checkdiagonalwin(){
        return ((checkrowcol(board[0][0], board[1][1], board[2][2]) == true) || checkrowcol(board[0][2],board[1][1] , board[2][0]) == true);
    }

    boolean checkwin(){
        return (checkrowwin() || checkcolwin() || checkdiagonalwin());
    }
    
    boolean placemark(int row, int col) {
        if ((row >= 0) && (row < 3)) {
            if ((col >= 0) && (col < 3)) {
                if (board[row][col] == '-') {
                    board[row][col] = playermark;
                    return true;
                }
            }
        }
        return false;
    }

    public char getPlayerMark() {
        return playermark;
}
void printBoard() {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            System.out.print(board[i][j] + "-");
        }
        System.out.println();
    }
}
    
}